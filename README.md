# vue3-ts-vite-template

#### Description
This is a template repository that can be fork when you want to create an application by vue3,typescript,vite.What's more,there are also some plugins inside——axios,sass,unocss,unplugin-auto-Import,unplugin-vue-router,iconfy,pritter,eslint,husky,vite-plugin-mock,mockjs,vite-plugin-layouts,pinia,vueuse etc.All in all, the purpose of creating this template is to make project development more automated.

#### Software Architecture
Software architecture description

#### Installation

1.  pnpm i
2.  npm i

#### Instructions

1.  npm run dev : start the server of development
2.  npm run format : format all the files with prettier
3.  npm run build : compile and build the project from vue to html & js

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
