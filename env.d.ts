/// <reference types="vite/client" />
/// <reference types="vite-plugin-vue-layouts/client" />

interface ImportMetaEnv {
  /** baseURL */
  readonly VITE_BASE_URL: string
  /** api接口的前缀 */
  readonly VITE_API_PREFIX: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}