import { IncomingMessage, ServerResponse } from 'http';
import type { MockMethod } from 'vite-plugin-mock';
import Mock from 'mockjs';
import path from 'path';
import fs from 'fs';

const prefix = '/api/v1';

// 函数形式导出
// export default function (config: MockConfig): MockMethod[] {
//   return [
//     {
//       url: '/api/test',
//       method: 'post',
//       rawResponse: async (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
//         let reqbody = '';
//         await new Promise((resolve) => {
//           req.on('data', (chunk) => {
//             reqbody += chunk;
//           });
//           req.on('end', () => resolve(undefined));
//         });
//         res.setHeader('Content-Type', 'text/plain');
//         res.statusCode = 200;
//         res.end(`hello, ${reqbody}`);
//       }
//     }
//   ];
// }

// 数组形式导出
export default [
  {
    // 接口测试get:http://localhost:5173/api/v1/test1
    url: `${prefix}/test1`,
    method: 'get',
    response: () => {
      const data = Mock.mock({
        'name|1': ['张三', '李四', '王五'],
        email: '@email',
        body: '@cparagraph'
      });
      return {
        code: 200,
        data,
        message: 'success'
      };
    }
  },
  {
    // 接口测试post:http://localhost:5173/api/v1/test2(需要使用接口测试工具如postman)
    url: `${prefix}/test2`,
    method: 'post',
    response: () => {
      const data = Mock.mock({
        'name|1': ['张三', '李四', '王五'],
        email: '@email',
        body: '@cparagraph'
      });
      return {
        code: 200,
        data,
        message: 'success'
      };
    }
  },
  {
    // 图片资源接口模拟:http://localhost:5173/api/v1/image/demo.png
    url: `${prefix}/image/:imageName`,
    method: 'get',
    rawResponse: async (req: IncomingMessage, res: ServerResponse<IncomingMessage>) => {
      const imageName = req.url?.replace(`${prefix}/image/`, '');
      const imagePath = path.join(__dirname, 'assets', imageName as string);

      fs.readFile(imagePath, (err, data) => {
        if (err) {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'text/plain');
          res.end('Error: Unable to load image');
        } else {
          res.setHeader('Content-Type', 'image/jpeg');
          res.statusCode = 200;
          res.end(data);
        }
      });
    }
  }
] as MockMethod[];
