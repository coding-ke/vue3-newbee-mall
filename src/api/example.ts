import axios from '@/utils/axios';

export const getExample = () => {
  return axios.post('/test2').then((res) => res.data);
};
