// 测试用例举例，待完善
import { describe, it, expect } from 'vitest';
import { shallowMount } from '@vue/test-utils';

import HelloWorld from '../HelloWorld.vue';
import { beforeEach } from 'node:test';

type imageTypeType = 'default' | 'rounded' | 'avatar';

const imageTypeValue: imageTypeType = 'default';

const props = {
  title: '',
  subTitle: '',
  titleClass: '',
  imageType: imageTypeValue
  // ...
};

let wrapperInstance;

describe('HelloWorld.vue', () => {
  beforeEach(() => {
    wrapperInstance = shallowMount(HelloWorld, {
      propsData: props
    });
  });

  // it定义单元测试，可以带参数
  it('HelloWorld组件测试用例1', () => {
    // 测试逻辑
    const title = 'HelloWorld';
    const subTitle = 'HelloWorld';
    const wrapper = shallowMount(HelloWorld, {
      propsData: {
        title,
        subTitle
      }
    });
    // 断言
    expect(wrapper.text()).toBe(title);
    expect(wrapper.text()).toBe(subTitle);
  });
});
