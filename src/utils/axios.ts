import axios from 'axios';
import type { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

class AxiosUtility {
  private instance: AxiosInstance;

  /**
   * Creates a new AxiosUtility instance with the specified baseURL and timeout configuration.
   *
   * @param {string} baseURL - The base URL for the requests.
   * @param {number} timeout - The timeout value for the requests in milliseconds.
   */
  constructor(baseURL: string, timeout: number) {
    this.instance = axios.create({
      baseURL,
      timeout
    });

    // Add request interceptor
    this.instance.interceptors.request.use(
      (config) => {
        // Add any custom logic here
        console.log('this.instance.interceptors.request', config);
        return config;
      },
      (error) => {
        // Handle request error here
        return Promise.reject(error);
      }
    );

    // Add response interceptor
    this.instance.interceptors.response.use(
      (response: AxiosResponse) => {
        // Add any custom logic here
        console.log('this.instance.interceptors.response', response);
        return response;
      },
      (error) => {
        // Handle response error here
        return Promise.reject(error);
      }
    );
  }

  /**
   * Sends a GET request to the specified URL.
   *
   * @param {string} url - The URL to send the request to.
   * @param {AxiosRequestConfig} config - Optional configuration for the request.
   * @return {Promise<any>} The response data.
   */
  get(url: string, config?: AxiosRequestConfig): Promise<any> {
    return this.instance.get(url, config);
  }

  /**
   * Sends a POST request to the specified URL.
   *
   * @param {string} url - The URL to send the request to.
   * @param {any} data - The data to send in the request body.
   * @param {AxiosRequestConfig} config - Optional configuration for the request.
   * @return {Promise<any>} The response data.
   */
  post(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
    return this.instance.post(url, data, config);
  }

  /**
   * Sends a PUT request to the specified URL.
   *
   * @param {string} url - The URL to send the request to.
   * @param {any} data - The data to send in the request body.
   * @param {AxiosRequestConfig} config - Optional configuration for the request.
   * @return {Promise<any>} The response data.
   */
  put(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
    return this.instance.put(url, data, config);
  }

  /**
   * Sends a DELETE request to the specified URL.
   *
   * @param {string} url - The URL to send the request to.
   * @param {AxiosRequestConfig} config - Optional configuration for the request.
   * @return {Promise<any>} The response data.
   */
  delete(url: string, config?: AxiosRequestConfig): Promise<any> {
    return this.instance.delete(url, config);
  }

  public getInstance(): AxiosInstance {
    return this.instance;
  }
}

const axiosUtility = new AxiosUtility(
  `${import.meta.env.VITE_BASE_URL}${import.meta.env.VITE_API_PREFIX}`,
  10000
);

export default axiosUtility.getInstance();
