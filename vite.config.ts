import { fileURLToPath, URL } from 'node:url';

import { ConfigEnv, defineConfig, UserConfig } from 'vite';
// import { UserConfigExport, ConfigEnv } from 'vite';

import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import AutoImport from 'unplugin-auto-import/vite';
import VueRouter from 'unplugin-vue-router/vite';
import { VueRouterAutoImports } from 'unplugin-vue-router';
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from 'unplugin-vue-components/resolvers';
import UnoCSS from 'unocss/vite';
import Layouts from 'vite-plugin-vue-layouts';
import { viteMockServe } from 'vite-plugin-mock';

// https://vitejs.dev/config/
// export default defineConfig({
//   plugins: [
//     VueRouter({
//       /* options */
//     }),
//     // ⚠️ Vue must be placed after VueRouter()
//     vue(),
//     vueJsx(),
//     UnoCSS(),
//     Layouts({
//       pagesDirs: 'src/pages',
//       layoutsDirs: 'src/layouts',
//       defaultLayout: 'default'
//     }),
//     AutoImport({
//       // targets to transform
//       include: [
//         /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
//         /\.vue$/,
//         /\.vue\?vue/, // .vue
//         /\.md$/ // .md
//       ],

//       // global imports to register
//       imports: [
//         // presets
//         'vue',
//         // 'vue-router'
//         VueRouterAutoImports,
//         '@vueuse/core'
//       ]
//     }),
//     Components({
//       resolvers: [VantResolver()],
//       dts: true, // enabled by default if `typescript` is installed
//       // Allow subdirectories as namespace prefix for components.
//       directoryAsNamespace: true,
//       // Collapse same prefixes (camel-sensitive) of folders and components
//       // to prevent duplication inside namespaced component name.
//       // works when `directoryAsNamespace: true`
//       collapseSamePrefixes: true
//     })
//   ],
//   resolve: {
//     alias: {
//       '@': fileURLToPath(new URL('./src', import.meta.url))
//     }
//   }
// });

export default defineConfig(({ command, mode }: ConfigEnv): UserConfig => {
  return {
    plugins: [
      VueRouter({
        /* options */
      }),
      // ⚠️ Vue must be placed after VueRouter()
      vue(),
      vueJsx(),
      UnoCSS(),
      Layouts({
        pagesDirs: 'src/pages',
        layoutsDirs: 'src/layouts',
        defaultLayout: 'default'
      }),
      AutoImport({
        // targets to transform
        include: [
          /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
          /\.vue$/,
          /\.vue\?vue/, // .vue
          /\.md$/ // .md
        ],

        // global imports to register
        imports: [
          // presets
          'vue',
          // 'vue-router'
          VueRouterAutoImports,
          '@vueuse/core'
        ]
      }),
      Components({
        resolvers: [VantResolver()],
        dts: true, // enabled by default if `typescript` is installed
        // Allow subdirectories as namespace prefix for components.
        directoryAsNamespace: true,
        // Collapse same prefixes (camel-sensitive) of folders and components
        // to prevent duplication inside namespaced component name.
        // works when `directoryAsNamespace: true`
        collapseSamePrefixes: true
      }),
      viteMockServe({
        // default
        mockPath: 'mock',
        // According to the project configuration. Can be configured in the .env file
        enable: true
      })
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    }
  };
});
