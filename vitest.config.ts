import { fileURLToPath } from 'node:url';
import { mergeConfig, defineConfig, configDefaults } from 'vitest/config';
import viteConfig from './vite.config';

export default defineConfig((configEnv) =>
  mergeConfig(
    viteConfig(configEnv),
    defineConfig({
      test: {
        environment: 'jsdom',
        exclude: [...configDefaults.exclude, 'cypress/*'],
        root: fileURLToPath(new URL('./', import.meta.url)),
        coverage: {
          provider: 'v8',
          enabled: true,
          reporter: ['text', 'json', 'html'],
          // all:true
          include: ['src/**/*.{vue,ts,tsx,js,jsx}'],
          exclude: ['src/**/__tests__/*']
        }
      }
    })
  )
);
